;; .emacs

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(diff-switches "-u")
 '(font-use-system-font t)
 '(fringe-mode (quote (2 . 0)) nil (fringe))
 '(global-visual-line-mode t)
 '(recentf-mode t)
 '(save-place t nil (saveplace))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(truncate-lines t)
 '(uniquify-buffer-name-style (quote forward) nil (uniquify)))

;;Cursor como una sola linea, al estilo de cualquier editor
(set-default 'cursor-type '(bar . 1))

;;CUA Mode on
(cua-mode 1)

;;Enable emacs to copy and paste to/from the clipboard
(setq x-select-enable-clipboard t)

;;How to have emacs highlight text selections?
(transient-mark-mode 1) ; highlight text selection
(delete-selection-mode 1) ; delete seleted text when typing

;;How to show line numbers?
(global-linum-mode 1) ; display line numbers in margin. Emacs 23 only.

;;How to show the cursor's column position?
(column-number-mode 1)

;;How to make emacs stop creating those “backup~” files or those “#autosave#” files?
(setq make-backup-files nil) ; stop creating those backup~ files
(setq auto-save-default nil) ; stop creating those #autosave# files

;;How to enable xterm mouse integration?
(setq xterm-mouse-mode 1)

;;Add Fill-Column-Indicator to draw nice line at fill-column
(load-file "~/.emacs.d/Fill-Column-Indicator/fill-column-indicator.el")
(require 'fill-column-indicator)
(add-hook 'after-change-major-mode-hook 'fci-mode)

(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
